-- Megamix Themes, originally made by Dimpsuu, remade by Snu as a DynMusic module

DynMusic.mmt = {} -- module namespace
local mmt = DynMusic.mmt -- shorthand to prevent clusterfucks
mmt.cv_megamixthemes = CV_RegisterVar({"megamixthemes", "Off", 0, CV_OnOff})

mmt.themes = {}
local labels = {}

-- 1: invincibility
-- 2: growth
mmt.GetMusic = function(skin, themetype)
	mmt.themes[skin] = $ ~= nil and $ or {}
	
	if (themetype == 1) and mmt.themes[skin][1] -- check for invincibility
		return mmt.themes[skin][1]
	elseif (themetype == 2) and mmt.themes[skin][2] -- check for growth
		return mmt.themes[skin][2]
	end
	-- they dont have anything
	return nil
end

DynMusic.FuckYou("addinvincibilitytheme", function(player, skin, lump, label)
	if (player ~= consoleplayer) then return end
	
	if not (skin) then
		CONS_Printf(player, "addinvincibilitytheme <skin> <lump>: Sets the invincibility theme for the given skin to the provided lump")
		return
	end
	
	if not (lump) then error("no music lump provided") return end
	mmt.themes[skin] = $ ~= nil and $ or {}
	
	mmt.themes[skin][1] = lump:upper()
	if (label) then
		-- initialize
		labels[skin] = $ ~= nil and $ or {}
		
		-- same with the labels table
		labels[skin][1] = label
	end
	CONS_Printf(player, "[Megamix Themes] Invincibility theme for \""..skin.."\" has been set to \x82"..lump.."\x80")
end)

DynMusic.FuckYou("addgrowththeme", function(player, skin, lump, label)
	if (player ~= consoleplayer) then return end
	
	if not (skin) then
		CONS_Printf(player, "addgrowththeme <skin> <lump>: Sets the growth theme for the given skin to the provided lump")
		return
	end
	
	if not (lump) then error("no music lump provided") return end
	mmt.themes[skin] = $ ~= nil and $ or {}
	
	mmt.themes[skin][2] = lump:upper()
	if (label) then
		-- initialize
		labels[skin] = $ ~= nil and $ or {}
		
		-- same with the labels table
		labels[skin][2] = label
	end
	CONS_Printf(player, "[Megamix Themes] Growth theme for \""..skin.."\" has been set to \x82"..lump.."\x80")
end)

COM_AddCommand("delmegamixthemes", function(player, skin)
	if (player ~= consoleplayer) then return end
	
	if not (skin) then
		CONS_Printf(player, "delmmt.themes <skin>: Removes the currently set megamix themes for the provided skin")
		return
	end
	
	mmt.themes[skin] = nil
	if (labels[skin]) then
		labels[skin] = nil
	end
	CONS_Printf(player, "[Megamix Themes] Megamix themes for \""..skin.."\" removed")
end)

COM_AddCommand("listmegamixthemes", function(player)
	if (player ~= consoleplayer) then return end
	
	if (next(mmt.themes) == nil) then
		CONS_Printf(player, "Megamix themes table is empty")
		return
	end
	
	CONS_Printf(player, "Megamix Theme List")
	
	for k, v in pairs(mmt.themes) do
		-- initialize tables
		v = $ ~= nil and $ or {}
		labels[k] = $ ~= nil and $ or {}
		
		-- create the starting string
		local str = ""
		
		-- check for invinc
		if v[1] 
			str = k.." (Invincibility): "..v[1]
			if (labels[k][1]) then
				str = str.." - "..labels[k][1]
			end
			CONS_Printf(player, str)
		end
		
		-- check for growth
		if v[2]
			str = k.." (Growth): "..v[2]
			if (labels[k][2]) then
				str = str.." - "..labels[k][2]
			end
			CONS_Printf(player, str)
		end
	end
end)

addHook("MusicChange", function(oldmusic, newmusic, flags, looping)
	if not (mmt.cv_megamixthemes.value) then return end
	
	local index = (newmusic == "kinvnc") and 1 or 2
	local mmt_player, mmt_best = nil, 0
	
	-- slight hack to find the player for splitscreen support
	for cp in displayplayers.iterate -- this is my new favourite thing in 1.2
		local mmt_checks = {k_invincibilitytimer, k_growshrinktimer}
		if not cp.kartstuff[mmt_checks[index]] then continue end
		
		-- check what displayplayer has the highest timer
		-- easy way of telling who just activated their item
		if (cp.kartstuff[mmt_checks[index]] > mmt_best)
			mmt_player = cp -- store them for later
			mmt_best = cp.kartstuff[mmt_checks[index]]
		end
	end
	
	-- if we got a player we do stuff
	if (mmt_player)
		local p = mmt_player
		
		if mmt.GetMusic(p.mo.skin, index)
			return mmt.GetMusic(p.mo.skin, index), flags, true
		end
	end
end)