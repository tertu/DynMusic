-- Victory themes, by zxyspku and Lat'
-- rewritten and probably ruined by Snu

DynMusic.vict = {} -- module namespace
local vict = DynMusic.vict -- shorthand to prevent clusterfucks
vict.cv_victorythemes = CV_RegisterVar({"victorythemes", "Off", 0, CV_OnOff})

vict.themes = {}
local labels = {}

-- exposing this so modders can get victory themes
vict.GetMusic = function(p)
	local skin = p.mo.skin
	vict.themes[skin] = $ ~= nil and $ or {} -- initialize
	
	if (p.kartstuff[k_position] == 1)
		if vict.themes[skin][1]
			return vict.themes[skin][1]
		else
			if vict.themes[skin][2]
				return vict.themes[skin][2]
			end
		end
		
		return "KRWIN"
	else
		if vict.themes[skin][2]
			return vict.themes[skin][2]
		else
			if vict.themes[skin][1]
				return vict.themes[skin][1]
			end
		end
		
		return "KROK"
	end
end

DynMusic.FuckYou("addvictorytheme", function(player, skin, lump, label)
	if (player ~= consoleplayer) then return end
	
	if not (skin) then
		CONS_Printf(player, "addvictorytheme <skin> <lump>: Sets the victory theme for the given skin to the provided lump")
		return
	end
	
	if not (lump) then error("no music lump provided") return end
	
	vict.themes[skin] = $ ~= nil and $ or {}
	
	vict.themes[skin][1] = lump:upper()
	if (label) then
		-- initialize
		labels[skin] = $ ~= nil and $ or {}
		
		-- same with the labels table
		labels[skin][1] = label
	end
	CONS_Printf(player, "[Victory Themes] Victory theme for \""..skin.."\" has been set to \x82"..lump.."\x80")
end)

DynMusic.FuckYou("addokayfinishtheme", function(player, skin, lump, label)
	if (player ~= consoleplayer) then return end
	
	if not (skin) then
		CONS_Printf(player, "addokayfinishtheme <skin> <lump>: Sets the okay victory theme for the given skin to the provided lump")
		return
	end
	
	if not (lump) then error("no music lump provided") return end
	
	vict.themes[skin] = $ ~= nil and $ or {}
	
	-- we'll store this in the second index, instead
	vict.themes[skin][2] = lump:upper()
	if (label) then
		labels[skin] = $ ~= nil and $ or {}
		labels[skin][2] = label
	end
	CONS_Printf(player, "[Victory Themes] Okay finish theme for \""..skin.."\" has been set to \x82"..lump.."\x80")
end)

COM_AddCommand("delvictorytheme", function(player, skin)
	if (player ~= consoleplayer) then return end
	
	if not (skin) then
		CONS_Printf(player, "delvictorytheme <skin>: Removes the currently set victory themes for the provided skin")
		return
	end
	
	vict.themes[skin] = nil
	if (labels[skin]) then
		labels[skin] = nil
	end
	CONS_Printf(player, "[Victory Themes] Victory themes for \""..skin.."\" removed")
end)

COM_AddCommand("listvictorythemes", function(player)
	if (player ~= consoleplayer) then return end
	
	if (next(vict.themes) == nil) then
		CONS_Printf(player, "Victory theme table is empty")
		return
	end
	
	CONS_Printf(player, "Victory Theme List")
	
	for k, v in pairs(vict.themes) do
		-- initialize tables
		v = $ ~= nil and $ or {}
		labels[k] = $ ~= nil and $ or {}
		
		-- create the starting string
		local str = ""
		
		-- check if 1st place victory theme exists
		if vict.themes[k][1] 
			str = k.." (Victory): "..v[1]
			if (labels[k][1]) then
				str = str.." - "..labels[k][1]
			end
			CONS_Printf(player, str)
		end
		
		-- check if okay finish theme exists
		if v[2]
			str = k.." (Okay Finish): "..v[2]
			if (labels[k][2]) then
				str = str.." - "..labels[k][2]
			end
			CONS_Printf(player, str)
		end
	end
end)

addHook("MusicChange", function(oldmusic, newmusic, flags, looping)
	if not (vict.cv_victorythemes.value) then return end
	if not ((newmusic == "krwin") or (newmusic == "krok")) then return end
	
	local vict_player
	for cp in displayplayers.iterate
		if cp.exiting
			vict_player = cp
		end
	end
	
	if (vict_player) -- paranoia
		local p = vict_player
		
		if vict.GetMusic(p)
			return vict.GetMusic(p), flags, true
		end
	end
end)